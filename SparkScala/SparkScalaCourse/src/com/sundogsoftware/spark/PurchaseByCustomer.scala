import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

/** Compute the total amount spent per customer in some fake e-commerce data. */
object PurchaseByCustomer{
  
    /** Convert input data to (customerID, customerPurchase) tuples */
     def perseLine(line: String) = {
       val fields = line.split(",")
       val customerID = fields(0).toInt
       val customerPurchase = fields(2).toFloat
       (customerID, customerPurchase)
       
     }  
  
  def main(args: Array[String]){
    
    // Set thr log level to only print error
    Logger.getLogger("org").setLevel(Level.ERROR)
    // create a SparkContext using every core of the local machine
    val sc = new SparkContext("local[*]", "PurchaseByCustomer")
    // 
    val line = sc.textFile("../customer-orders.csv")
    
    val pursedLines = line.map(perseLine)
    
    val totalAmountPurchased = pursedLines.reduceByKey((x, y)=> (x + y))
    
    val results = totalAmountPurchased.collect()
    // print the results
    results.foreach(println)
    
   }
  }
