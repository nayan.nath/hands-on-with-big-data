//package com.sundogsoftware.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object PurchaseByCustomerSorted {
  
  def parsedLine(line:String) = {
    
    val fields = line.split(",")
    val customerID = fields(0).toInt
    val customerTransaction = fields(2).toFloat
    (customerID, customerTransaction)
  }
  
  
  
  def main(args: Array[String]){
    
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val sc = new SparkContext("local[*]", "PurchaseByCustomerSorted")
    
    val line = sc.textFile("../customer-orders.csv")
    
    val parseLines = line.map(parsedLine)
    
    val totalTransactionById = parseLines.reduceByKey((x, y) => x+y)
    
    val resultsFilpped = totalTransactionById.map(x=>(x._2, x._1))
    
    /*
    val totalResult = resultsFilpped.collect()   
    for (result <- totalResult.sorted){
      
      val customerIdSorted = result._2
      val customerTransactionSorted = result._1
      
      println(s"$customerIdSorted transaction is: $customerTransactionSorted")
    }
    */
    
    // Another way top do this
    
    val customerTransactionSorted = resultsFilpped.sortByKey()

    val results = customerTransactionSorted.collect()
    
    results.foreach(println)
  
  }
}
