package com.sundogsoftware.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.io.Codec
import scala.math.sqrt

object MovieSimilarities {
  
  
  // Load up a Map of movie ID's to movie names
  def loadMovieNames(): Map[Int, String] = {
    // Handle character encoding issues:
    implicit val codec = Codec("UTF-8")
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)
    
    // Create a Map of Ints to String, and populate it fromu.item.
    var movieNames: Map[Int, String] = Map()
    
    val lines = Source.fromFile("../ml-100k/u.item").getLines()
    
    for (line<-lines) {
      var fields = line.split("|")
      if (fields.length> 1) {
        movieNames += (fields(0).toInt -> fields(1))
      }
    }
    
    return movieNames
    
  }
  
  type MovieRating = (Int, Double)
  type UserRatingPair = (Int, (MovieRating, MovieRating))
  def makePairs(userRatings:UserRatingPair) = {
    val movieRating1 = userRatings._2._1
    val movieRating2 = userRatings._2._2
    
    val movie1 = movieRating1._1
    val rating1 = movieRating1._2
    val movie2 = movieRating2._1
    val rating2 = movieRating2._2
    
    ((movie1, movie2), (rating1, rating2))
  }
  
  def filterDuplicates(userRatings:UserRatingPair):Boolean = {
    val movieRating1 = userRatings._2._1
    val movieRating2 = userRatings._2._2
    
    val movie1 = movieRating1._1
    val movie2 = movieRating2._1
    
    return movie1 < movie2
  }
  
  type RatingPair = (Double, Double)
  type RatingPairs = Iterable[RatingPair]
  
  def computeCosineSimilarity(ratingPairs: RatingPairs): (Double, Int) = {
    
    var numPairs:Int = 0
    var sum_xx:Double = 0.0
    var sum_yy:Double = 0.0
    var sum_xy:Double = 0.0
    
    for(pair <- ratingPairs) {
      val ratingX = pair._1
      val ratingY = pair._2
      
      sum_xx += ratingX * ratingX
      sum_yy += ratingY * ratingY
      sum_xy += ratingX * ratingY
      
      numPairs +=1
    }
    
    val numerator:Double = sum_xy
    val denominator = sqrt(sum_xx) * sqrt(sum_yy)
    
    var score:Double = 0.0
    if (denominator != 0) {
      score = numerator / denominator
    }
    
    return (score, numPairs)
    
  }
  
  
  
  
  
  
  
  def main(arfs: Array[String]) = {
   
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    val sc = new SparkContext("local[*]", "MovieSimilarities")
    
    println("\n Loading Movie Names ...")
    val nameDict = loadMovieNames()
    
    val data = sc.textFile("../ml-100k/u.data")
    
    // Map rating to key/value pairs: userID => ((movieID, rating), (movieID, rating))
    val ratings = data.map(l => l.split("\t")).map(l => (l(0).toInt, (l(1).toInt, l(2).toDouble)))
    
    // Emit every movie rated together by the same user.
    // Self join to find every combination
    val joinedRatings = ratings.join(ratings)
    
    // at this point our RDD consosts of userID => ((movieID, rating), (movieID, rating))
    
    // Filter out the duplicate pairs
    val uniqueJoinedRating = joinedRatings.filter(filterDuplicates)
    
    // Now key by (movie1, movie2) pairs
    val moviePairs = uniqueJoinedRating.mapValues(computeCosineSimilarity)
    
    
    
    
    
  }
}