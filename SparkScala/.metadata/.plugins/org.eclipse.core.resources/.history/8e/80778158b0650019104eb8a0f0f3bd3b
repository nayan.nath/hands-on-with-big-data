package com.sundogsoftware.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.rdd._
import org.apache.spark.util.LongAccumulator
import org.apache.log4j._
import scala.collection.mutable.ArrayBuffer

/** Find the degrees of seperation between two Marvel comic book characters, based of 
* co-appearances in a comic.
*/

object DegreeOfSeperation {
  
  // The character we want to find the sepration between
  val startCharacterID = 5306 // SpiderMan
  val targetCharacterID = 14 // ADAM, 3031 (who?)
  
  // We make our accumulator a "global" Option so we can reference it in a mapper later
  var hitCounter: Option[LongAccumulator] = None
  
  // Some custom data types
  // BFSData contains an array of hero ID connections, the distance and color
  type BFSData = (Array[Int], Int, String)
  // A BFSNode has a heroID and the BFSData associated with it.
  type BFSNode = (Int, BFSData)
  
  // Create a line of raw input into a BFSNode */
  def convertToBFS(line: String): BFSNode = {
    // Split up the line into fields
    val fields = line.split("\\s+")
    
    // Extract this hero ID from the first field
    val heroID = fields(0).toInt
    
    // Extract subsequent hero ID's into the connection array
    var connections: ArrayBuffer[Int] = ArrayBuffer()
 
    for ( connection <- 1 to (fields.length - 1)) {
      connections += fields(connection).toInt
    }
    
    // Default distance and color is 9999 abd white
    var color: String = "WHITE"
    var distance: Int = 9999
    
    // Unless this is character we're starting from
    if(heroID == startCharacterID) {
      
      color = "GRAY"
      distance = 0
    }
    
    return (heroID, (connections.toArray, distance, color))
    
  }
  
  
  
  /** Create "iteration 0" of our RDD of BFSNodes */
  def createStartingRdd(sc:SparkContext): RDD[BFSNode] = {
    val inputFile = sc.textFile("../marvel-graph.txt")
    return inputFile.map(convertToBFS)
  }
  
  // Expands a BFSNode into this node ands it's children */
  def bfsMap(node: BFSNode): Array[BFSNode] = {
    
    // Extract data from BFSNode
    val characterID :Int = node._1
    val data:BFSData = node._2
    
    val connections: Array[Int] = data._1
    val distance:Int = data._2
    val color: String = data._3
    
    // This is called from faltMap, so we return an array of potentially many BFSNode to add to our new RDD 
    val results: ArrayBuffer[BFSNode] = ArrayBuffer()
    
    // Gray node are flagged for expansion and create new gray nodes for each connection
    if(color== "GRAY"){
      for(connection<- connections){
        val newCharacterID = connection
        val newDistance = distance + 1
        val newColor = "GRAY"
        
        // Have we stumbled across the character we're looking for?
        // If so increament our accumulator so the driver script knows
        if(targetCharacterID == connection) {
          if (hitCounter.isDefined) {
            hitCounter.get.add(1)
          }
        }
        
        // Create our new Gray node for this connection and add it to the results
        val newEntry:BFSNode = (newCharacterID, (Array(), newDistance, newColor))
        results += newEntry
      }
    }
    
  }
  

 
  
  
  
  
  
  /** Our main function where the action happens */
  def main(args: Array[String]) {
    // Set the log level to only print error
    Logger.getLogger("org").setLevel(Level.ERROR)
    
    // create a SparkContext using every core of the local machine
    val sc = new SparkContext("local[*]", "DegreeOfSeperation")
    
    // Our accumulator, used to signal when we find the target
    // Character in our BFS traversal.
    hitCounter = Some(sc.longAccumulator("Hit Counter"))
    
    var iterationRdd = createStartingRdd(sc)
    
    var iteration: Int = 0
    for(iteration<- 1 to 10) {
      println(" Running BFS Iteration # " + iteration)
      
      // Create new vertices as needed to darken or reduce distances in the reduce stage. If we encounter
      // the node we're looking for as a GRAY node, increment our accumulator to signal that we're done.
      
      val mappped = iterationRDdd.flatMap(bfsMap)
      
      
      
    }
       
    
  }
  
}

